package skiplist

import (
	"math/rand"
	"sort"
	"strconv"
	"testing"
)

func LTEQ(l, r interface{}) bool {
	return l.(int) <= r.(int)
}

func TestInsertSimple(t *testing.T) {

	sl := New(LTEQ)

	if sl.Len() != 0 {
		t.Errorf("Length not zero")
	}

	sl.Insert(4, "four")
	if sl.Len() != 1 {
		t.Errorf("Length not one")
	}

	sl.Insert(4, "4")
	if sl.Len() != 1 {
		t.Errorf("Length not one")
	}

	sl.Insert(6, "six")
	if sl.Len() != 2 {
		t.Errorf("Length not two")
	}

	sl.Insert(6, "6")
	if sl.Len() != 2 {
		t.Errorf("Length not two")
	}

	sl.Insert(1, "one")

	Next := NextIterator(sl)

	if Next() != "one" {
		t.Errorf("value not 'one'")
	}
	if Next() != "4" {
		t.Errorf("value not '4'")
	}
	if Next() != "6" {
		t.Errorf("value not '6'")
	}
	if Next() != nil {
		t.Errorf("value not nil")
	}
}

func TestInsertSimple_2(t *testing.T) {
	sl := New(LTEQ)

	sl.Insert(6, "six")
	sl.Insert(4, "four")

	Next := NextIterator(sl)

	if Next() != "four" {
		t.Errorf("value not 'four'")
	}

	if Next() != "six" {
		t.Errorf("value not 'four'")
	}
}

func TestInsertSimple_3(t *testing.T) {
	sl := New(LTEQ)

	sl.Insert(4, "four")
	sl.Insert(6, "six")
	sl.Insert(10, "ten")

	Next := NextIterator(sl)

	if Next() != "four" {
		t.Errorf("value not 'four'")
	}

	if Next() != "six" {
		t.Errorf("value not 'six'")
	}
	if Next() != "ten" {
		t.Errorf("value not 'ten'")
	}
}

func TestInsertRandom(t *testing.T) {

	ranbuf := make([]int, 25000)
	for i, _ := range ranbuf {
		ranbuf[i] = rand.Intn(4294967295)
	}

	sl := New(LTEQ)
	for _, k := range ranbuf {
		sl.Insert(k, strconv.Itoa(k))
	}

	sort.Ints(ranbuf)
	if uint32(len(ranbuf)) != sl.Len() {
		t.Errorf("buffer and list not equal length")
	}

	Next := NextIterator(sl)
	for i, j := range ranbuf {
		nv := Next()
		rv := strconv.Itoa(j)
		if rv != nv {
			t.Errorf("value differ between ranbuf & list\n@ %d "+
				"newbuf value: %v, list value: %v", i, rv, nv)
		}
	}
}

func TestLookup(t *testing.T) {
	sl := New(LTEQ)

	if sl.Lookup(123) != nil {
		t.Errorf("value not nil")
	}

	sl.Insert(123, "123")
	if sl.Lookup(123) != "123" {
		t.Errorf("value not '123'")
	}

	if sl.Lookup(456) != nil {
		t.Errorf("value not nil")
	}

	sl.Insert(456, "456")
	if sl.Lookup(456) != "456" {
		t.Errorf("value not '456'")
	}

	if sl.Lookup(789) != nil {
		t.Errorf("value not nil")
	}

	sl.Insert(123, "onetwothree")
	if sl.Lookup(123) != "onetwothree" {
		t.Errorf("value not 'onetwothree'")
	}

	sl.Insert(456, "fourfivesix")
	if sl.Lookup(456) != "fourfivesix" {
		t.Errorf("value not 'fourfivesix'")
	}

	sl.Insert(789, "seveneightnine")
	if sl.Lookup(789) != "seveneightnine" {
		t.Errorf("value not 'seveneightnine'")
	}

	sl.Insert(789, "789")
	if sl.Lookup(789) != "789" {
		t.Errorf("value not '789")
	}
}

func TestLookup_2(t *testing.T) {
	sl := New(LTEQ)

	sl.Insert(123, "123")
	if sl.Lookup(123) != "123" {
		t.Errorf("value not '123'")
	}

	sl.Insert(123, "onetwothree")
	if sl.Lookup(123) != "onetwothree" {
		t.Errorf("value not 'onetwothree'")
	}

	sl.Insert(123, "one23")
	if sl.Lookup(123) != "one23" {
		t.Errorf("value not 'one23'")
	}
}

func TestLookup_3(t *testing.T) {
	sl := New(LTEQ)
	sl.Insert(111, "111")
	sl.Insert(123, "123")
	if sl.Lookup(123) != "123" {
		t.Errorf("value not '123'")
	}

	sl.Insert(123, "onetwothree")
	if sl.Lookup(123) != "onetwothree" {
		t.Errorf("value not 'onetwothree'")
	}

	sl.Insert(123, "one23")
	if sl.Lookup(123) != "one23" {
		t.Errorf("value not 'one23'")
	}
}

func TestLookupRandom(t *testing.T) {

	ranbuf := make([]int, 25000)
	for i := range ranbuf {
		ranbuf[i] = rand.Intn(4294967295)
	}

	sl := New(LTEQ)
	for _, k := range ranbuf {
		sl.Insert(k, strconv.Itoa(k))
	}

	//sort.Ints(ranbuf)
	if uint32(len(ranbuf)) != sl.Len() {
		t.Errorf("lookup, buffer and list not equal length")
	}

	for i, j := range ranbuf {
		sv := sl.Lookup(j)
		rv := strconv.Itoa(j)
		if rv != sv {
			t.Errorf("value differ between ranbuf & lookup\n@ %d "+
				"ranbuf value: %v, lookup value: %v", i, rv, sv)
		}
	}
}

func TestInsertAndDelete(t *testing.T) {
	sl := New(LTEQ)

	if sl.Delete(123) != nil {
		t.Errorf("value not nil")
	}

	sl.Insert(9, "nine")
	if sl.Delete(9) != "nine" {
		t.Errorf("value not 'nine'")
	}

	if sl.Len() != 0 {
		t.Errorf("length not zero")
	}

	if sl.Delete(1) != nil {
		t.Errorf("value not nil")
	}

	sl.Insert(2, "two")
	sl.Insert(10, "ten")

	if sl.Delete(2) != "two" {
		t.Errorf("value not 'two'")
	}

	if sl.Len() != 1 {
		t.Errorf("length not 1")
	}

	sl.Insert(2, "two")

	if sl.Delete(10) != "ten" {
		t.Errorf("value not 'ten'")
	}

	if sl.Len() != 1 {
		t.Errorf("length not 1")
	}
	sl.Insert(71, "sevenone")
	sl.Insert(93, "ninethree")

	if sl.Delete(46) != nil {
		t.Errorf("value not nil")
	}
	if sl.Delete(71) != "sevenone" {
		t.Errorf("value not 'sevenone'")
	}
}

func TestInsertAndDeleteRandom(t *testing.T) {
	ranbuf := make([]int, 25000)
	rembuf := make([]string, 0)

	for i, _ := range ranbuf {
		ranbuf[i] = rand.Intn(4294967295)
	}

	sl := New(LTEQ)

	for _, k := range ranbuf {
		sl.Insert(k, strconv.Itoa(k))
	}

	sort.Ints(ranbuf)
	for i := 0; i < len(ranbuf); i++ {
		if !(i%3 == 0) {
			rembuf = append(rembuf, strconv.Itoa(ranbuf[i]))
		} else {
			sl.Delete(ranbuf[i])
		}
	}

	if sl.Len() != uint32(len(rembuf)) {
		t.Errorf("length of list and rembuf not equal")
	}

	Next := NextIterator(sl)

	for i := 0; i < len(rembuf); i++ {
		val := Next()
		if rembuf[i] != val {
			t.Errorf("list and rembuf value mismatch")
		}
	}
}

func BenchmarkInsert(b *testing.B) {
	/* run as: 'go test -bench . '
	   or: go test -bench=Benchmark_Insert for this test only
	*/

	if testing.Short() {
		b.Skip("skipping test in short mode.")
	}

	ranbuf := make([]int, b.N)
	for i := range ranbuf {
		ranbuf[i] = rand.Intn(4294967295)
	}

	b.ResetTimer()
	sl := New(LTEQ)

	for _, k := range ranbuf {
		sl.Insert(k, strconv.Itoa(k))
	}

}

func BenchmarkLookup(b *testing.B) {
	if testing.Short() {
		b.Skip("skipping test in short mode.")
	}

	ranbuf := make([]int, b.N)
	for i := range ranbuf {
		ranbuf[i] = rand.Intn(4294967295)
	}

	sl := New(LTEQ)

	for _, k := range ranbuf {
		sl.Insert(k, strconv.Itoa(k))
	}

	b.ResetTimer()

	for j := len(ranbuf) - 1; j >= 0; j-- {
		sl.Lookup(ranbuf[j])
	}
}

func BenchmarkDelete(b *testing.B) {
	if testing.Short() {
		b.Skip("skipping test in short mode.")
	}

	ranbuf := make([]int, b.N)
	for i := range ranbuf {
		ranbuf[i] = rand.Intn(4294967295)
	}

	sl := New(LTEQ)

	for _, k := range ranbuf {
		sl.Insert(k, strconv.Itoa(k))
	}

	b.ResetTimer()

	for j := len(ranbuf) - 1; j >= 0; j-- {
		sl.Delete(ranbuf[j])
	}

	if sl.Len() != 0 {
		b.Errorf("benchmark list not empty")
	}
}

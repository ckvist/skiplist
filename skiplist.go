// Copyright (C) 2014 Cristoffer Kvist. All rights reserved.
// This project is licensed under the terms of the MIT license in LICENSE.

// Package skiplist provides a skip list implementation based on W. Pugh's work
package skiplist

import (
	"fmt"
	"math"
	"math/rand"
)

const (
	Height int = 32
)

type node struct {
	key, value interface{}
	next       []*node
}

type skipList struct {
	// head points to the sentinel created at the construction of new list
	head *node

	// update holds a record of nodes at each level that might need to be
	// updated at the insertion of a new node
	update []*node

	// lteq is the supplied 'less than or equal' func for the key type
	lteq func(l, r interface{}) bool

	// size is the number of nodes currently in the list
	size uint32
}

func New(lteq func(l, r interface{}) bool) skipList {
	// Create an empty node with next pointers for the full height
	sentinel := &node{next: make([]*node, Height)}

	// Create the update slice of full height
	up := make([]*node, Height)

	return skipList{head: sentinel, update: up, lteq: lteq}
}

// randHeight returns a integer N with probability distribution 1/(2^N)
// N:[                1                ][        2        ][    3    ] ... [32]
// p:|<------------- 0.5 ------------->||<---- 0.25 ----->||<-0.125->| ...
func randHeight() int {
	randFloat := rand.Float64()
	randHeight := int(math.Ceil(math.Log(randFloat) / math.Log(0.5)))

	if randHeight >= Height || randHeight < 0 {
		return Height
	}
	return randHeight
}

func (lst *skipList) Insert(key, val interface{}) {
	newNodeHeight := randHeight()

	// Create new node to be inserted
	n := node{key: key, value: val}
	n.next = make([]*node, newNodeHeight)

	ptr := lst.head
	update := lst.update

	// Find insertion point
	for level := Height - 1; level >= 0; level-- {
		for ; ptr.next[level] != nil; ptr = ptr.next[level] {
			if lst.lteq(key, ptr.next[level].key) {
				break
			}
		}
		update[level] = ptr
	}

	if ptr.next[0] == nil { // Node will be inserted last in the list
		for level := newNodeHeight - 1; level >= 0; level-- {
			update[level].next[level] = &n
		}
		lst.size++
	} else if key == ptr.next[0].key { // Replace node value only
		ptr.next[0].value = val
	} else { // Insert in between two nodes
		for level := newNodeHeight - 1; level >= 0; level-- {
			n.next[level] = update[level].next[level]
			update[level].next[level] = &n
		}
		lst.size++
	}
}

func (lst skipList) Lookup(key interface{}) interface{} {
	ptr := lst.head

	// Find node
	for level := Height - 1; level >= 0; level-- {
		for ; ptr.next[level] != nil; ptr = ptr.next[level] {
			if lst.lteq(key, ptr.next[level].key) {
				break
			}
		}
	}

	if ptr.next[0] != nil && ptr.next[0].key == key {
		return ptr.next[0].value
	}
	return nil
}

func (lst *skipList) Delete(key interface{}) interface{} {
	ptr := lst.head
	update := lst.update

	for level := Height - 1; level >= 0; level-- {
		for ; ptr.next[level] != nil; ptr = ptr.next[level] {
			if lst.lteq(key, ptr.next[level].key) {
				break
			}
		}
		update[level] = ptr
	}

	if ptr.next[0] != nil && ptr.next[0].key == key {
		value := ptr.next[0].value

		for level := len(ptr.next[0].next) - 1; level >= 0; level-- {
			update[level].next[level] = ptr.next[0].next[level]
		}
		lst.size--
		return value
	}
	return nil
}

func (lst skipList) Len() uint32 {
	return lst.size
}

// Iterator returning a function that returns the next value in a list
func NextIterator(lst skipList) func() interface{} {
	node := lst.head.next[0]
	return func() interface{} {
		if node != nil {
			value := node.value
			node = node.next[0]
			return value
		}
		return nil
	}
}

// PrintTree outputs the skiplist as a vertical diagram with node address,
// key, value and the next pointers to the following nodes
func (lst skipList) PrintTree() {
	node := lst.head

	fmt.Printf("Head_ptr:    %p  Length: %d", node, lst.size)
	for i := 0; i < Height; i++ {

		if (i % 8) == 0 {
			fmt.Printf("\n            ")
		}
		fmt.Printf("[%7.2d     ]", i)
	}
	fmt.Println()

	for index := 0; node != nil; index++ {
		height := len(node.next)
		fmt.Println()

		for i := 0; i < 5; i++ {
			switch i {
			case 0:
				fmt.Printf("Addr___Next: ")
			case 1:
				fmt.Printf("%p ", node)
			case 2:
				fmt.Printf("Node: %.6x ", index)
			case 3:
				fmt.Printf("Key: %7v ", node.key)
			case 4:
				fmt.Printf("Value: %v ", node.value)
				continue
			}

			for j := i * 8; j < 8*i+8 && j < height; j++ {
				fmt.Printf("%10.10p  ", node.next[j])
			}
			fmt.Println()
		}
		fmt.Println()
		node = node.next[0]
	}
	fmt.Println()
}
